package com.example.medico.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medico.R;
import com.example.medico.model.Ticket;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

public class TiketReservasi extends AppCompatActivity {
    Toolbar toolbar;

    String email_key = "";
    String email_key_new = "";
    int pageWidth = 1200;
    TextView TVNamaRS, TVMedicalCheckUp, TVHargaPelayanan, TVTanggalPelayanan, TVWaktuPelayanan;
    Button btn_cetak;
    private Bitmap myBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiket_reservasi);



        Intent before = getIntent();
        getUsernameLocal();
        toolbar = (Toolbar) findViewById(R.id.ToolbarTiketReservasi);

        TextView toolbar_text = findViewById(R.id.toolbar_text);
        final TextView TVNamaRS = findViewById(R.id.TVNamaRS);
        final TextView TVMedicalCheckUp = findViewById(R.id.TVMedicalCheckUp);
        final TextView TVHargaPelayanan = findViewById(R.id.HargaPelayanan);
        final TextView TVTanggalPelayanan = findViewById(R.id.TanggalPelayanan);
        final TextView TVWaktuPelayanan = findViewById(R.id.WaktuPelayanan);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text.setText("Tiket Reservasi");

        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE},PackageManager.PERMISSION_GRANTED);

        final String IdTicket = before.getStringExtra(getString(R.string.IdTicket));

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Tiket").child(email_key_new).child(IdTicket);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                TVNamaRS.setText(dataSnapshot.child("penyedia_layanan").getValue().toString());
                TVMedicalCheckUp.setText(dataSnapshot.child("jns_layanan").getValue().toString());
                TVHargaPelayanan.setText(dataSnapshot.child("harga_layanan").getValue().toString());
                TVTanggalPelayanan.setText(dataSnapshot.child("tanggal").getValue().toString());
                TVWaktuPelayanan.setText(dataSnapshot.child("waktu").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
        final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.RL_tiket);
        relativeLayout.post(new Runnable() {
            public void run() {
                relativeLayout.setDrawingCacheEnabled(true);
                //take screenshot
                myBitmap = captureScreen(relativeLayout);

//                Toast.makeText(getApplicationContext(), "Screenshot captured..!", Toast.LENGTH_LONG).show();

                try {
                    if (myBitmap != null) {
                        //save image to SD card
                        saveImage(myBitmap);
                    }
//                    Toast.makeText(getApplicationContext(), "Screenshot saved..!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });

    }

    public Bitmap captureScreen(View v) {
        View rv = v.getRootView();
        Bitmap screenshot = null;
        try {

            if (v != null) {

                screenshot = Bitmap.createBitmap(rv.getMeasuredWidth(), rv.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(screenshot);
                v.draw(canvas);
            }

        } catch (Exception e) {
            // Log.d("ScreenShotActivity", "Failed to capture screenshot because:" + e.getMessage());
        }

        return screenshot;
    }
    public void saveImage(Bitmap bitmap) throws IOException {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, bytes);
        File f = new File(Environment.getExternalStorageDirectory()  + File.separator  + "tiket123.jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f, false);
        fo.write(bytes.toByteArray());
        fo.close();
    }

    private void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences("id", MODE_PRIVATE);
        email_key_new = sharedPreferences.getString(email_key,"");
    }

    public void cetaktiket(View view) throws FileNotFoundException {
        imageToPDF();
    }

    public void createPDF() {

                TVNamaRS = findViewById(R.id.TVNamaRS);
                TVHargaPelayanan = findViewById(R.id.HargaPelayanan);
                TVMedicalCheckUp = findViewById(R.id.TVMedicalCheckUp);
                TVTanggalPelayanan = findViewById(R.id.TanggalPelayanan);
                TVWaktuPelayanan = findViewById(R.id.WaktuPelayanan);
                btn_cetak = findViewById(R.id.btn_cetak);


                PdfDocument pdf = new PdfDocument();
                Paint myPaint = new Paint();
                Paint tittlePaint = new Paint();

                PdfDocument.PageInfo myPageInfo1 = new PdfDocument.PageInfo.Builder(1200, 2010, 1).create();
                PdfDocument.Page myPage1 = pdf.startPage(myPageInfo1);
                Canvas canvas = myPage1.getCanvas();

                tittlePaint.setTextAlign(Paint.Align.CENTER);
                tittlePaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                tittlePaint.setTextSize(70);
                canvas.drawText("E-TICKET :", pageWidth / 2, 270, tittlePaint);

                myPaint.setTextAlign(Paint.Align.LEFT);
                myPaint.setTextSize(35f);
                myPaint.setColor(Color.BLACK);
                canvas.drawText("Customer : " + TVNamaRS.getText(), 20, 590, myPaint);

                pdf.finishPage(myPage1);

                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/Hello.pdf");

                try {
                    if (!file.exists()) {
                        // file does not exist, create it
                        file.createNewFile();
                        pdf.writeTo(new FileOutputStream(file));
                        Toast.makeText(getApplicationContext(),"BERHASIL MENYIMPAN TIKET!",Toast.LENGTH_LONG).show();
                    }else{
                        pdf.writeTo(new FileOutputStream(file, false));
                        Toast.makeText(getApplicationContext(),"BERHASIL MENYIMPAN TIKET!",Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                pdf.close();
            }



    public void imageToPDF() throws FileNotFoundException {
        String dirpath;
        try {
            Document document = new Document();
            dirpath = String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
            PdfWriter.getInstance(document, new FileOutputStream(dirpath + "/Tikettt.pdf")); //  Change pdf's name.
            document.open();
            Image img = Image.getInstance(Environment.getExternalStorageDirectory() + File.separator + "tiket123.jpg");
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / img.getWidth()) * 100;
            img.scalePercent(scaler);
            img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            document.add(img);
            document.close();
            Toast.makeText(this, "PDF Generated successfully!..", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }


}
